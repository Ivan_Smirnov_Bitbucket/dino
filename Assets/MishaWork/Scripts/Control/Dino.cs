﻿using System;
using System.Collections.Generic;
using System.Timers;
using Exploder;
using Exploder.Utils;
using UnityEditor;
using UnityEngine;
using Plane = UnityEngine.Plane;

namespace Dino
{
    [RequireComponent(typeof(Jumper))]
    public class Dino : MonoBehaviour
    {
        [Header("Effects data")]
        [SerializeField] private CameraShake cameraShake;
        [SerializeField] private TrailRenderer leftJumpTrail;
        [SerializeField] private TrailRenderer rightJumpTrail;
        [SerializeField] private ParticleSystem confettyParticles;
        [Header("Trajectory")]
        [SerializeField] private TrajectoryRenderer trajectoryRenderer;
        [SerializeField] private Transform defaultTrajectoryTarget;
        [SerializeField] private Transform minTrajectoryTransform;
        [SerializeField] private Camera touchInputCamera;
        [SerializeField] private Transform finalPointDebug;
        [SerializeField] private JumpIndicator jumpIndicator;
        [Header("Mesh data")]
        [SerializeField] private Transform dinoBody;
        [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
        [SerializeField] private MeshRenderer brushSkinnedRenderer;
        [SerializeField] private GameObject hiddenDinoForAnimation;
        [Header("Jump parameters")]
        [SerializeField] private float power = 100;
        [SerializeField] private float velocityMultipliyer = 10;
        [SerializeField] private float trajectoryMovementSpeed = 10;
        [SerializeField] private float touchToleranceY = 3.5f;
        [SerializeField] private float touchToleranceX = 3.5f;
        [Header("Destroy trigger")]
        [SerializeField] private DinoCollisionTrigger destroyTrigger;
        
        public List<GameObject> objectsToExplode = new List<GameObject>();

        public CameraFollow followCamera;

        private Vector3 _mouseInWorld;
        private Vector3 _speed;

        RaycastHit hit;
        private RaycastHit2D raycastHit2D;
        private Ray ray;
        private Plane plane;
        private float enter;

        private bool _isPlayerTouch;

        private Animator _animator;

        private Camera _cameraToCount;
        private Vector3 startTrajectoryPoint;

        private bool _firstTimeColliderWithGound = true;
        public bool canJump = true;
        public bool dieWithJump;
        public bool _canMoveTrajectory = false;
        
        //input info
        private Vector2 enterInputPoint;
        private Vector2 startInputPoint;
        private Vector2 endInputPoint;
        private float startTime;
        private float enterTime;
        private float endTime;
        private float swipeSpeed;

        private Collider dinoFootCollider;
        private float startVelocityMultipliyer;
        
        //falling
        private float currentHeight;
        private float previousHeight;
        private bool isJumping;
        private Rotator jumpParticlesRotator;

        private Transform brushParent;

        private Jumper jumper;

        private void Awake()
        {
            confettyParticles.Stop();
            startVelocityMultipliyer = velocityMultipliyer;
            jumpParticlesRotator = leftJumpTrail.GetComponent<Rotator>();
            jumper = GetComponent<Jumper>();

            _cameraToCount = followCamera.GetComponent<Camera>();
            _animator = GetComponent<Animator>();
            dinoFootCollider = GetComponent<BoxCollider>();
            
            startTrajectoryPoint = defaultTrajectoryTarget.transform.localPosition;
            plane = new Plane(Vector3.up, Vector3.zero);
        }

        public void PlayRoarAnimation()
        {
            TurnConfetty(true);
            followCamera.target = null;
            followCamera.isFixedPosition = true;
            GameController.instance.currentState = GameState.CompleteLevel;
            canJump = false;
            
            skinnedMeshRenderer.enabled = false;
            brushSkinnedRenderer.enabled = false;
            
            hiddenDinoForAnimation.gameObject.SetActive(true);
            hiddenDinoForAnimation.GetComponent<Animator>().SetBool("isRoaring", true);
        }

        public void TurnConfetty(bool turn)
        {
            if (turn) confettyParticles.Play();
            else confettyParticles.Stop();
        }

        private void Update()
        {
            if (!canJump || GameController.instance.currentState == GameState.CompleteLevel) return;

            InputCheck();
            SetTrajectoryBounds();
            RotatePlayerCharacterTowardsTargetPosition();
        }

        private void InputCheck()
        {
            if (Input.GetMouseButtonDown(0))
            {
                DetectFirstTouch();
                DrawTrajectory();
                velocityMultipliyer = startVelocityMultipliyer;
            }

            if (Input.GetMouseButton(0))
            {
                jumpIndicator.gameObject.SetActive(true);
                DetectSwipe();
                DrawTrajectory();
                MoveTrajectoryHorizontally(GetSwipeDirectionNormalized(enterInputPoint, GetInputWorldPosition()));
            }

            if (Input.GetMouseButtonUp(0))
            {
                DetectSwipe();
                if (canJump)
                {
                    jumper.Jump(transform.position, defaultTrajectoryTarget.transform.position);
                    GraphicsJumpUpdate();
                }
                canJump = false;
                isJumping = true;
                
                defaultTrajectoryTarget.transform.localPosition = startTrajectoryPoint;
                
                trajectoryRenderer.lineRenderer.positionCount = 0;

                jumpIndicator.gameObject.SetActive(false);
            }
        }

        private void GraphicsJumpUpdate()
        {
            leftJumpTrail.emitting = true;
            rightJumpTrail.emitting = true;
            
            dinoBody.GetComponent<Animator>().SetBool("isJumping",true);
            _animator.SetTrigger("jumping");
            _animator.ResetTrigger("startJump");

            if (dieWithJump)
            {
                followCamera.target = null;
                velocityMultipliyer += 10;
            }
        }

        private void DrawTrajectory()
        {
            Vector3 targetDir = defaultTrajectoryTarget.position - transform.position;
            
            _speed = targetDir * power;
            _speed.y = defaultTrajectoryTarget.localPosition.z * 0.5f;

            bool inBounds;
            Vector3 finalPoint;

            trajectoryRenderer.ShowTrajectory(new Vector3(transform.position.x, transform.position.y + 0.5f,
                transform.position.z), _speed, out inBounds, out finalPoint);
            
            finalPointDebug.transform.position = finalPoint;

            if (inBounds)
            {
                dieWithJump = false;
                jumpIndicator.ChangeSprite(true);
            }
            else
            {
                dieWithJump = true;
                jumpIndicator.ChangeSprite(false);
            }
        }

        public void TurnAnimation(string animationName)
        {
            _animator.SetTrigger(animationName);
        }

        private void RotatePlayerCharacterTowardsTargetPosition()
        {
            Quaternion newRotation = Quaternion.LookRotation(defaultTrajectoryTarget.transform.position -
                                                             transform.position);
            newRotation.x = 0;
            newRotation.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, 0.5f);
        }

        private void MoveTrajectoryPoint(Vector2 direction, float speed)
        {
            if (direction == Vector2.zero) return;

            direction.y *= -1;

            if (_canMoveTrajectory)
            {
                Vector3 position = defaultTrajectoryTarget.transform.localPosition;
                position += new Vector3(0, 0, direction.y) * 
                            (swipeSpeed * trajectoryMovementSpeed * Time.deltaTime);
                position.y = position.z * 0.5f;
                
                defaultTrajectoryTarget.transform.localPosition = position;
            }
        }

        private void MoveTrajectoryHorizontally(Vector2 direction)
        {
            float horizontalSpeed = GetHorizontalSpeed(enterTime, Time.time, enterInputPoint,
                GetInputWorldPosition());
            
            Vector3 rotation = transform.localRotation.eulerAngles;
            rotation.y += direction.x * horizontalSpeed * 1000 * Time.deltaTime;
                
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotation.y, 0),
                5 * Time.deltaTime);
        }

        private void SetTrajectoryBounds()
        {
            Vector3 trajectoryTargetPosition = defaultTrajectoryTarget.transform.localPosition;

            trajectoryTargetPosition.x = Mathf.Clamp(trajectoryTargetPosition.x, -Mathf.Infinity,
                Mathf.Infinity);
            trajectoryTargetPosition.y = Mathf.Clamp(trajectoryTargetPosition.y, 7f, Mathf.Infinity);
            trajectoryTargetPosition.z = Mathf.Clamp(trajectoryTargetPosition.z, 5f, Mathf.Infinity);

            defaultTrajectoryTarget.transform.localPosition = trajectoryTargetPosition;
        }

        private void DetectSwipe()
        {
            endInputPoint = GetInputWorldPosition();
            endTime = Time.time;

            float swipeDistance = Vector2.Distance(startInputPoint, endInputPoint);
            float timeDifference = endTime - startTime;
            swipeSpeed = swipeDistance / timeDifference;

            Vector2 directionNormalized = GetSwipeDirectionNormalized(startInputPoint, endInputPoint);
            
            startTime = endTime;
            startInputPoint = endInputPoint;
            
            CheckForCanMoveTrajectory();
            
            MoveTrajectoryPoint(directionNormalized, swipeSpeed);
        }

        public void CheckForCanMoveTrajectory()
        {
            //_canMoveTrajectory = !(swipeSpeed <= 0.001);
        }

        public float GetHorizontalSpeed(float startTime, float endTime, Vector2 startPoint, Vector2 endPoint)
        {
            float swipeDistance = Vector2.Distance(startPoint, endPoint);
            float timeDifference = endTime / startTime;

            return (swipeDistance / timeDifference);
        }

        public Vector2 GetSwipeDirectionNormalized(Vector2 startPoint, Vector2 endPoint) => 
            (startPoint - endPoint).normalized;


        private void DetectFirstTouch()
        {
            _firstTimeColliderWithGound = false;
            
            ResetAllTriggers();
            _animator.SetTrigger("startJump");
            
            enterInputPoint = GetInputWorldPosition();
            enterTime = Time.time;
            startInputPoint = GetInputWorldPosition();
            startTime = Time.time;
            
            jumpIndicator.gameObject.SetActive(true);
        }

        public Vector2 GetInputWorldPosition()
        {
            float x = Input.mousePosition.x;
            float y = Input.mousePosition.y;
            float z = -10;

            Vector2 mousePosition = touchInputCamera.ScreenToWorldPoint(new Vector3(x, y, z));

            return mousePosition;
        }

        public void RotateBodyToStartPosition()
        {
            dinoBody.GetComponent<Animator>().SetBool("isJumping",false);
        }

        public void ResetAllTriggers()
        {
            _animator.ResetTrigger("startJump");
            _animator.ResetTrigger("jumping");
            _animator.ResetTrigger("endJump");

            dinoBody.GetComponent<Animator>().SetBool("isJumping", false);
        }

        public void ReloadDino()
        {
            skinnedMeshRenderer.enabled = true;
            brushSkinnedRenderer.enabled = true;
            
            hiddenDinoForAnimation.GetComponent<Animator>().SetBool("isRoaring", false);
            hiddenDinoForAnimation.gameObject.SetActive(false);
            
            ResetAllTriggers();
            
            followCamera.ReturnTarget();
            followCamera.isFixedPosition = false;
            
            _firstTimeColliderWithGound = true;
            canJump = true;
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (!_firstTimeColliderWithGound && !canJump)
            {
                if (other.gameObject.CompareTag("Ground"))
                {
                    OnGroundHit();
                }
            }
        }

        private void OnGroundHit()
        {
            _animator.SetTrigger("endJump");
            leftJumpTrail.emitting = false;
            rightJumpTrail.emitting = false;
            dinoBody.GetComponent<Animator>().SetBool("isJumping", false);
            
            destroyTrigger.ExplodeObjects();

            isJumping = false;
            leftJumpTrail.transform.rotation = Quaternion.Euler(180, 0 , 0);
            
            canJump = true;

            followCamera.target = null;
            StartCoroutine(cameraShake.Shake(0.2f, 0.3f));
        }
    }
}