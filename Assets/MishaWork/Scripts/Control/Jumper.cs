﻿using System;
using UnityEngine;

namespace Dino
{
    public class Jumper : MonoBehaviour
    {
        [SerializeField] private float jumpForce;
        [SerializeField] private float velocityMultipliyer;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Jump(Vector3 from, Vector3 to)
        {
            Vector3 speed = (to - from) * jumpForce;
            
            Quaternion newRotation = Quaternion.LookRotation(to - from);
            newRotation.x = 0;
            newRotation.z = 0;
            transform.rotation = newRotation;

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.AddForce(new Vector3(speed.x, speed.y + 20, speed.z) *
                               velocityMultipliyer);
            
        }
    }
}