﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private Vector3 angleToRotate;
    private float _rotationSpeed;

    private float _rotationAmount;

    private bool _isRotating;
    private Vector3 _startRotation;

    private void Awake()
    {
        _startRotation = transform.localRotation.eulerAngles;
    }

    public void Rotate(Vector3 angle, float rotationSpeed)
    {
        angleToRotate = angle;
        _isRotating = true;
        _rotationSpeed = rotationSpeed;
    }

    public void RotateToStartPosition()
    {
        _isRotating = true;
        angleToRotate = _startRotation;
    }

    private void Update()
    {
        if (_isRotating)
        {
            if (Vector3.Distance(transform.eulerAngles, angleToRotate) > 0.01f)
            {
                transform.localEulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, angleToRotate,
                    Time.deltaTime * _rotationSpeed);
            }
            else
            {
                transform.eulerAngles = angleToRotate;
                _isRotating = false;
            }
        }

    }
}
