﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Exploder;
using Exploder.Utils;
using UnityEngine;
using UnityEngine.UIElements;

namespace Dino
{
    public class DinoCollisionTrigger : MonoBehaviour
    {
        [SerializeField] private ExploderObject dinamicExploder;
        [SerializeField] private ExploderObject smallObjectsStaticExploder;
        [SerializeField] private ExploderObject bigObjectsStaticExploder;
        
        public bool staticTrigger;
        public bool canExplode;
        public bool botTrigger;

        //private List<GameObject> _objectsToExplode = new List<GameObject>();

        private ExploderObject exploder;

        private Dino dinoParent;

        //private List<ExplodableObstacle> _smallObjectsToExplode;
        

        private void Awake()
        {
            exploder = ExploderSingleton.Instance;
            dinoParent = GetComponentInParent<Dino>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<ExplodableObstacle>())
            {
                if (botTrigger)
                {
                    if (other.GetComponentsInChildren<MonoBehaviour>().Length > 0)
                    {
                        CheckForChildren(other.gameObject);
                    }
                    
                    CheckForTagAndExplode(other.GetComponent<ExplodableObstacle>());
                }
                else
                {
                    if (staticTrigger)
                    {
                        if (other.GetComponentsInChildren<MonoBehaviour>().Length > 0)
                        {
                            CheckForChildren(other.gameObject);
                        }
                    
                        dinoParent.objectsToExplode.Remove(other.gameObject);
                        Vector3 dinoParentRotation = dinoParent.transform.rotation.eulerAngles;
                        dinoParent.RotateBodyToStartPosition();
                    
                        CheckForTagAndExplode(other.GetComponent<ExplodableObstacle>());
                    }
                    else
                        dinoParent.objectsToExplode.Add(other.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<ExplodableObstacle>())
            {
                dinoParent.objectsToExplode.Remove(other.gameObject);
            }
        }

        public void ExplodeObjects()
        {
            if (dinoParent.objectsToExplode.Count <= 0) return;

            foreach (var objectToExplode in dinoParent.objectsToExplode)
            {
                if (objectToExplode)
                    SetDeathType(objectToExplode.GetComponent<ExplodableObstacle>());
            }

            dinamicExploder.ExplodeObjects(dinoParent.objectsToExplode.ToArray());
            
            dinoParent.objectsToExplode.Clear();
        }

        private void CheckForTagAndExplode(ExplodableObstacle objectToExplode)
        {
            string gameObjectTag = objectToExplode.gameObject.tag;

            SetDeathType(objectToExplode);

            switch (gameObjectTag)
            {
                case "Small Obstacle":
                    smallObjectsStaticExploder.ExplodeObject(objectToExplode.gameObject);
                    break;
                case "Obstacle":
                    smallObjectsStaticExploder.ExplodeObject(objectToExplode.gameObject);
                    break;
                case "Big Obstacle":
                    bigObjectsStaticExploder.ExplodeObject(objectToExplode.gameObject);
                    break;
            }

            dinoParent.objectsToExplode.Remove(objectToExplode.gameObject);


        }

        private void SetDeathType(ExplodableObstacle objectToExplode)
        {
            if (botTrigger)
                objectToExplode.SetDeathType(ExplodableObstacle.DeathType.Bot);
            else if (staticTrigger)
                objectToExplode.SetDeathType(ExplodableObstacle.DeathType.StaticTrigger);
            else if (!staticTrigger)
                objectToExplode.SetDeathType(ExplodableObstacle.DeathType.DinamicTrigger);
        }

        private void CheckForChildren(GameObject objectToExplode)
        {
            List<GameObject> children = new List<GameObject>();
            
            foreach (Transform child in objectToExplode.transform)
            {
                if (botTrigger)
                {
                    child.GetComponent<ExplodableObstacle>().SetDeathType(ExplodableObstacle.DeathType.Bot);
                }
                children.Add(child.gameObject);
            }

            smallObjectsStaticExploder.ExplodeObjects(children.ToArray());
            
        }

        /*
        private void OnTriggerStay(Collider other)
        {
            if (!canExplode) return;
            
            if (other.gameObject.GetComponent<ExplodableObstacle>())
            {
                GameController.instance.obstalces.Remove(other.gameObject.
                    GetComponent<ExplodableObstacle>());
                GameController.instance.ChangeBar();
                if (GameController.instance.obstalces.Count < 5) Debug.Log("Player is win!");
                ExploderSingleton.Instance.ExplodeObject(other.gameObject);
                //GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
        */

        /*
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.GetComponent<ExplodableObstacle>())
            {
                GameController.instance.obstalces.Remove(other.gameObject.
                    GetComponent<ExplodableObstacle>());
                GameController.instance.ChangeBar();
                if (GameController.instance.obstalces.Count < 5) Debug.Log("Player is win!");
                ExploderSingleton.Instance.ExplodeObject(other.gameObject);
                //GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
        */
    } 
}

