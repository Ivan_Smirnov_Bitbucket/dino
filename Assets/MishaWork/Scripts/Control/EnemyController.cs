﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dino
{
    public class EnemyController : MonoBehaviour
    {
        public enum EnemyType
        {
            Simple, Medium, Hard
        }

        private string[] names = {"Babba", "Bobba", "Galaglab", "T-Rex", "Satanilla"};
        
        [SerializeField] private EnemyType enemyType;
        [SerializeField] private float jumpTimer = 5.0f;
        [SerializeField] private TextMeshPro name;

        private float _timer;

        private Jumper _jumper;
        private bool _canJump;
        private bool _wasOnGround;
        private Animator _animator;
        private Vector3 _startPosition;

        private void Awake()
        {
            name.text = names[Random.Range(0, names.Length)];
            
            _timer = jumpTimer;
            _jumper = GetComponent<Jumper>();
            _animator = GetComponent<Animator>();
            _startPosition = transform.position;
            
            _animator.SetTrigger("startJump");
        }

        void Update()
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0 && _canJump)
            {
                _jumper.Jump(transform.position, CountRandomJumpVector());
                _animator.SetTrigger("jumping");
                _canJump = false;
                _timer = jumpTimer;
            }
        }

        private Vector3 CountRandomJumpVector()
        {
            Vector3 randomJumpVector;
            
            randomJumpVector = new Vector3(Random.Range(-3f, 3f), 2.5f, Random.Range(-3f, 3f));

            return randomJumpVector;
        }

        public void ReturnOnStartPoint()
        {
            transform.position = _startPosition;
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (!_canJump)
            {
                if (other.gameObject.CompareTag("Ground"))
                {
                    _canJump = true;
                    _animator.SetTrigger("endJump");
                    _animator.SetTrigger("startJump");
                }
            }
        }
    }
}

