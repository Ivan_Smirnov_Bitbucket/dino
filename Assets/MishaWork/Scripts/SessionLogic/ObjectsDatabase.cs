﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Dino
{
    public class ObjectsDatabase : MonoBehaviour
    {
        public static ObjectsDatabase instance;

        public Transform textPopupPrefab;

        private void Awake()
        {
            instance = this;
        }
        
    }
}