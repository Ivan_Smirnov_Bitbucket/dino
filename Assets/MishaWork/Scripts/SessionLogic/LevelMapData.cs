﻿using UnityEngine;

namespace Dino
{
    [System.Serializable]
    public class LevelMapData
    {
        public string levelName;
        public GameObject levelGameObject;
        public GameObject UISpawnPoints;
        public Transform[] spawnPoints;
        public ExplodableObstacle[] obstacles;
    }
}