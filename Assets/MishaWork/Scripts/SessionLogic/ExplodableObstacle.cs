﻿using System;
using Exploder;
using Exploder.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dino
{
    public class ExplodableObstacle : MonoBehaviour
    {
        public enum DeathType
        {
            StaticTrigger, DinamicTrigger, Bot
        }
        public int cost = 100;

        private TextPopup _damageText;

        private bool _isDoubledRewardedDeath;
        private bool _deathFromBot;

        private void Start()
        {
            cost = Random.Range(20, 41);
            Transform text = Instantiate(ObjectsDatabase.instance.textPopupPrefab,
                new Vector3(transform.position.x, transform.position.y + 1, transform.position.z),
                Quaternion.identity);
            _damageText = text.GetComponent<TextPopup>();
            _damageText.gameObject.SetActive(false);
            
            GameController.instance.obstaclesCount++;
            GameController.instance.obstalces.Add(this);
        }

        private void OnDestroy()
        {
            if (_deathFromBot)
            {
                Destroy(_damageText.gameObject);
                
                GameController.instance.destroyedObstacles++;
                GameController.instance.ChangeBar();
                GameController.instance.CheckForEndOfSession();
            }
            else
            {
                int reward = CheckForTagAndGiveReward();
            
                if (_isDoubledRewardedDeath) reward *= 2;

                if (_damageText)
                {
                    _damageText.gameObject.SetActive(true);
                    _damageText.canMove = true;
                    _damageText.SetUp(GetAnimationTag(), "+ $ " + reward.ToString());
                }
            
                GameController.instance.AddPoints(reward);
                GameController.instance.destroyedObstacles++;
                GameController.instance.ChangeBar();
                GameController.instance.CheckForEndOfSession();
            }
        }

        private string GetAnimationTag()
        {
            string animationTag;
            
            switch (gameObject.tag)
            {
                case "Small Obstacle":
                    animationTag = "smallFade";
                    break;
                case "Obstacle":
                    animationTag = "mediumFade";
                    break;
                case "Big Obstacle":
                    animationTag = "largeFade";
                    break;
                default:
                    animationTag = "smallFade";
                    break;
            }

            return animationTag;
        }

        private int CheckForTagAndGiveReward()
        {
            int reward = 0;
            
            switch (gameObject.tag)
            {
                case "Small Obstacle":
                    reward = Random.Range(20, 41);
                    break;
                case "Obstacle":
                    reward = Random.Range(50, 76);
                    break;
                case "Big Obstacle":
                    reward = Random.Range(100, 201);
                    break;
            }

            return reward;
        }

        public void SetDeathType(DeathType deathType)
        {
            switch (deathType)
            {
                case DeathType.StaticTrigger:
                    _isDoubledRewardedDeath = false;
                    break;
                case DeathType.DinamicTrigger:
                    _isDoubledRewardedDeath = true;
                    break;
                case DeathType.Bot:
                    _deathFromBot = true;
                    break;
            }
        }

    }
}