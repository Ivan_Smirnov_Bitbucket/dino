﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dino;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private Text levelCaption;
    
    public GameObject currentLevelMap;
    public GameObject currentUIPoints;

    public void LoadLevel(LevelMapData levelMapData)
    {
        levelCaption.text = levelMapData.levelName;
        
        currentLevelMap.SetActive(false);
        currentLevelMap = levelMapData.levelGameObject;
        currentLevelMap.SetActive(true);
        
        currentUIPoints.SetActive(false);
        currentUIPoints = levelMapData.UISpawnPoints;
        currentUIPoints.SetActive(true);
    }
}
