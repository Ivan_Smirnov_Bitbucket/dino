﻿using UnityEngine;
using System.Collections;

namespace Dino
{
    public class CameraShake : MonoBehaviour
    {
        public IEnumerator Shake(float duration, float magnitude)
        {
            Vector3 orignalPosition = transform.position;
            float elapsed = 0f;
        
            while (elapsed < duration)
            {
                float x = Random.Range(-1f, 1f) * magnitude;
                float y = Random.Range(-1f, 1f) * magnitude;
                float z = Random.Range(-1f, 1f) * magnitude;
                
                transform.position += new Vector3(x, y, z);
                
                elapsed += Time.deltaTime;
                yield return 0;
            }
            
            transform.position = orignalPosition;
            GetComponent<CameraFollow>().ReturnTarget();
        }
    }
}
