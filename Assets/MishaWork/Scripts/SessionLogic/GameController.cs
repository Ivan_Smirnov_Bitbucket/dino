﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dino
{
    public enum GameState
    {
        PointChoose,
        AnimationScene,
        ActionScene,
        CompleteLevel
    }
    public class GameController : MonoBehaviour
    {
        public static GameController instance;
        
        public GameState currentState = GameState.PointChoose;
        
        [Header("UI elements")]
        [SerializeField] private Text progressCaption;
        [SerializeField] private Text levelNumberCaption;
        [SerializeField] private Text levelThirdViewCaption;

        [Header("Points panel")] 
        [SerializeField] private GameObject pointsPanel;
        [SerializeField] private TextMeshProUGUI pointsText;
        
        [SerializeField] private int currentLevelIndex = 0;
        [SerializeField] private LevelManager levelManager;
        [SerializeField] private LevelMapData[] levelMapsPresenters;
        [SerializeField] private Transform[] spawnPoints;
        [SerializeField] private ObjectsWithDinoPresenter startObjects;
        [SerializeField] private ObjectsWithDinoPresenter actionObjects;
        [SerializeField] private ObjectsWithDinoPresenter thirdViewObjects;
        [SerializeField] private Image progressBarImage;
        [SerializeField] private bool debugNextLevel = false;
        public List<ExplodableObstacle> obstalces;

        public int points;
        
        public int obstaclesCount = 0;
        public int destroyedObstacles = 0;
        
        public Dino dino;
        
        public Transform textPopupLookPoint;
        public Transform dinoSpawnPoint;

        private float maxObstacles;

        private GameObject currentLevelMap;
        private bool wasDeadOnThisLevel = false;
        
        private void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(gameObject);
        }

        private void Start()
        {
            levelManager.LoadLevel(levelMapsPresenters[currentLevelIndex]);

            currentLevelMap = levelMapsPresenters[currentLevelIndex].levelGameObject;
            
            levelNumberCaption.text = "LEVEL " + (currentLevelIndex + 1).ToString();
            levelThirdViewCaption.text = "LEVEL " + (currentLevelIndex + 1).ToString();
            progressCaption.text = 0 + "%";
        }

        public void CheckForEndOfSession()
        {
            if (progressCaption == null) return;
            
            progressCaption.text = Mathf.RoundToInt(100 *
                                                     (destroyedObstacles / maxObstacles))
                .ToString() + "%";
            
            if (destroyedObstacles == obstaclesCount)
            {
                dino.PlayRoarAnimation();
                StartCoroutine(ChangeLevelWithDelay());
            }
            
            SetProgressBarXScale((destroyedObstacles/maxObstacles));
        }

        public void AddPoints(int amount)
        {
            points += amount;
            
            pointsPanel.GetComponent<Animator>().SetTrigger("increase");
            pointsText.text = "$ " + points.ToString();
        }
        

        public void ChoosePointToDino(int pointIndex)
        {
            dinoSpawnPoint = spawnPoints[pointIndex];
            
            if (!wasDeadOnThisLevel)
                ChangeObjects(2);
            else ChangeObjects(3);
        }

        public void ChangeLevel()
        {
            if (!debugNextLevel) return;

            obstaclesCount = 0;
            SetProgressBarXScale(0);

            wasDeadOnThisLevel = false;
            
            levelManager.LoadLevel(levelMapsPresenters[currentLevelIndex]);

            currentLevelMap.SetActive(false);

            currentLevelIndex++;

            if (currentLevelIndex > 5) return;
            
            levelNumberCaption.text = "LEVEL " + (currentLevelIndex + 1).ToString();
            levelThirdViewCaption.text = "LEVEL " + (currentLevelIndex + 1).ToString();
            progressCaption.text = 0 + "%";
            destroyedObstacles = 0;

            levelManager.LoadLevel(levelMapsPresenters[currentLevelIndex]);
            
            spawnPoints = levelMapsPresenters[currentLevelIndex].spawnPoints;
            if (currentLevelIndex >= levelMapsPresenters.Length) return;

            ChangeObjects(1);
            
            dino.ReloadDino();
            
            maxObstacles = obstaclesCount;
        }

        private void SetProgressBarXScale(float x)
        {
            if (!progressBarImage) return;
            
            var localScale = progressBarImage.transform.localScale;
            
            localScale = new Vector3(x, localScale.y, localScale.z);
            
            progressBarImage.transform.localScale = localScale;
        }

        public IEnumerator ReturnToStartPoint()
        {
            dino.TurnConfetty(false);
            dino.followCamera.target = null;
            yield return new WaitForSeconds(1.5f);
            
            ChangeObjects(1);
            
            dino.ResetAllTriggers();
            dino.GetComponent<Rigidbody>().velocity = Vector3.zero;
            dino.transform.position = dinoSpawnPoint.transform.position;
            dino.transform.rotation = dinoSpawnPoint.transform.rotation;

            wasDeadOnThisLevel = true;

            dino.followCamera.ReturnTarget();
            dino.canJump = true;
        }

        public void ChangeBar()
        {
            if (progressBarImage == null) return;
            
            var localScale = progressBarImage.transform.localScale;
            
            localScale = new Vector3(destroyedObstacles/ maxObstacles, 
                localScale.y, localScale.z);
            progressBarImage.transform.localScale = localScale;
        }

        private IEnumerator ChangeLevelWithDelay()
        {
            yield return new WaitForSeconds(4.7f);
            
            ChangeLevel();
        }

        public void ChangeObjects(int sessionIndex)
        {
            switch (sessionIndex)
            {
                case 1:
                    currentState = GameState.PointChoose;
                    startObjects.objects.SetActive(true);
                    actionObjects.objects.SetActive(false);
                    thirdViewObjects.objects.SetActive(false);
                    break;
                case 2:
                    currentState = GameState.AnimationScene;
                    startObjects.objects.SetActive(false);
                    actionObjects.objects.SetActive(true);
                    thirdViewObjects.objects.SetActive(false);
                    actionObjects.dino.GetComponent<Animator>().SetBool("isRoaring", true);
                    StartCoroutine(ChangeToActionSceneWithDelay());
                    levelManager.currentLevelMap.SetActive(false);
                    break;
                case 3:
                    currentState = GameState.ActionScene;
                    startObjects.objects.SetActive(false);
                    actionObjects.objects.SetActive(false);
                    dino.transform.position = dinoSpawnPoint.position;
                    dino.transform.rotation = dinoSpawnPoint.rotation;
                    thirdViewObjects.objects.SetActive(true);
                    levelManager.currentLevelMap.SetActive(true);
                    maxObstacles = obstaclesCount;
                    break;
            }
        }

        private IEnumerator ChangeToActionSceneWithDelay()
        {
            yield return new WaitForSeconds(4.3f);
            
            ChangeObjects(3);
        }
    }
}