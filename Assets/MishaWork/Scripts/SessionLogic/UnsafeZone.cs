﻿using UnityEngine;

namespace Dino
{
    public class UnsafeZone : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Dino>())
            {
                StartCoroutine(GameController.instance.ReturnToStartPoint());
            }

            if (other.GetComponent<EnemyController>())
            {
                other.GetComponent<EnemyController>().ReturnOnStartPoint();
            }
        }
    }
}