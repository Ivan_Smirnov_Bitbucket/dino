﻿ using System;
 using UnityEngine;
 using System.Collections;
 
 namespace Dino
 {
 public class CameraFollow : MonoBehaviour
 {
     public Transform target;
 
     [SerializeField] private Vector3 offsetPosition;
 
     [SerializeField] private Space offsetPositionSpace = Space.Self;
 
     [SerializeField] private bool lookAt = true;

     public bool isFixedPosition = false;
     public Transform fixedTarget = null;

     private Transform _startTarget;

     private void Awake()
     {
         _startTarget = target;
     }

     private void Update()
     {
         Refresh();
     }

     private void FixedUpdate()
     {
         MoveToFixedTarget();
     }

     public void SetFixedTarget(Transform target)
     {
         fixedTarget = target;
         isFixedPosition = true;
     }

     private void MoveToFixedTarget()
     {
         if (!isFixedPosition) return;
         
         transform.localPosition = Vector3.MoveTowards(transform.position, fixedTarget.position,
             Time.deltaTime * 10);
     }

     public void ReturnTarget()
     {
         target = _startTarget;
     }
 
     public void Refresh()
     {
         if (target == null) return;

         if (!isFixedPosition)
         {
             // compute position
             if (offsetPositionSpace == Space.Self)
             {
                 transform.position = target.TransformPoint(offsetPosition);
             }
             else
             {
                 transform.position = target.position + offsetPosition;
             }
         }

         // compute rotation
         if(lookAt)
         {
             Vector3 targetTransform;
             targetTransform = new Vector3(target.transform.position.x, target.transform.position.y,
                 target.transform.position.z);
             Vector3 direction = targetTransform - transform.position;
             Quaternion lookRotation = Quaternion.LookRotation(direction);
             transform.rotation = lookRotation;
         }
         else
         {
             transform.rotation = target.rotation;
             /*
             Quaternion targetRotation = target.rotation;
             targetRotation.x = 0;
             targetRotation.z = 0;
             transform.localRotation = targetRotation;
             */
         }
     }
    }
 }