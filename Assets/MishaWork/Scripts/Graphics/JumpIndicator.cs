﻿using System;
using UnityEngine;

namespace Dino
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class JumpIndicator : MonoBehaviour
    {
        [SerializeField] private Transform objectToRotate;
        [SerializeField] private Transform trajectorytargetCircle;
        [SerializeField] private Sprite yesSprite;
        [SerializeField] private Sprite noSprite;

        private SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            RotateToObject();
        }

        public void ChangeSprite(bool inBounds)
        {
            if (inBounds)
            {
                _spriteRenderer.sprite = null;
                trajectorytargetCircle.gameObject.SetActive(true);
            }
            else
            {
                _spriteRenderer.sprite = noSprite;
                trajectorytargetCircle.gameObject.SetActive(false);
            }
        }

        private void RotateToObject()
        {
            if (objectToRotate)
            {
                transform.LookAt(objectToRotate);
            }
        }
    }
}