﻿using System;
using TMPro;
using UnityEngine;

namespace Dino
{
    public class TextPopup : MonoBehaviour
    {
        [SerializeField] private bool moveUp = false;
        public bool canMove;
        
        private float dissapearTimer;
        private string text;
        private float alphaChangeDelta;

        private TextMeshPro popupText;
        private Color textColor;

        public void SetUp (string animationTag, string text)
        {
            GetComponent<Animator>().SetTrigger(animationTag);
            this.text = text;
            GetComponent<TextMeshPro>().text = text;
            textColor = GetComponent<TextMeshPro>().color;
            canMove = true;
        }

        private void Awake ()
        {
            popupText = GetComponent<TextMeshPro>();
        }

        private void Update()
        {
            if (canMove)
            {
                transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0);
                if (moveUp) transform.position += Vector3.up * (Time.deltaTime * 0.5f);
            }
        }

        public void DestroyTextPopup()
        {
            Destroy(this.gameObject);
        }
        
    }
}