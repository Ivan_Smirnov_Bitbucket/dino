﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dino
{
    public class TrajectoryRenderer : MonoBehaviour
    {
        public LineRenderer lineRenderer;

        private void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        public void ShowTrajectory(Vector3 origin, Vector3 speed, out bool inBounds, out Vector3 finalPoint)
        {
            Vector3[] points = new Vector3[100];

            int finalPointIndex = 0;

            for (int i = 0; i < points.Length; ++i)
            {
                float time = i * 0.1f;

                points[i] = origin + speed * time + Physics.gravity * (time * time) / 2f;
                
                if (points[i].y < 0)
                {
                    finalPointIndex = i;
                    lineRenderer.positionCount = i + 1;
                    break;
                }
            }
            
            inBounds = CheckForGround(points[finalPointIndex - 2],
                    points[finalPointIndex], out finalPoint);

            lineRenderer.SetPositions(points);
        }

        private bool CheckForGround(Vector3 startPoint, Vector3 endPoint, out Vector3 finalPoint)
        {
            RaycastHit raycastHit;

            Physics.Raycast(new Vector3(startPoint.x, startPoint.y + 5, startPoint.z ),
                Vector3.down, out raycastHit);

            if (raycastHit.collider != null)
            {
                if (raycastHit.collider.gameObject.tag == "Ground")
                {
                    finalPoint = new Vector3(raycastHit.point.x, raycastHit.point.y + 0.1f, raycastHit.point.z);
                    return true;
                }
            }

            finalPoint = new Vector3(endPoint.x, endPoint.y + 0.5f, endPoint.z);
            return false;
        }
    }
}

