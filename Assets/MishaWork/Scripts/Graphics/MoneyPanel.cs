﻿using System;
using UnityEngine;

namespace Dino
{
    public class MoneyPanel : MonoBehaviour
    {
        private bool _isAnimationPlay;

        private Vector3 startSize;
        private float decreasingSpeed;

        private RectTransform _transform;

        private void Awake()
        {
            startSize = transform.localScale;
            _transform = GetComponent<RectTransform>();
        }

        public void StartAnimation(float duration)
        {
            var localScale = _transform.localScale;
            
            localScale = new Vector3(localScale.x * 20,
                localScale.y * 20, localScale.z * 20);
            
            _transform.localScale = localScale;

            decreasingSpeed = duration * Time.deltaTime;
            _isAnimationPlay = true;
        }

        private void Update()
        {
            if (_isAnimationPlay)
            {
                var localScale = _transform.localScale;
                
                localScale = new Vector3(localScale.x - decreasingSpeed,
                    localScale.y * decreasingSpeed, localScale.z * decreasingSpeed);
                transform.localScale = localScale;

                if (_transform.localScale.x <= startSize.x)
                {
                    _transform.localScale = startSize;
                    _isAnimationPlay = false;
                }
            }
        }
    }
}